# node bird Clone

nextjs, node, mysql을 이용한 full stack app practice

- node:  version 14.x
- ant design verion 4.x !
- nextjs version 9.x !
- styled-component: 5.x
- next-redux-wrapper: 6.x! (Provider 하면 안됨. 이미 내부에서 처리함)

### ver 9.xxx
- typescript 가능
- dynamic routing
- static optimization
- 성능 최적화

## nextjs 
- react framework : react 보다 코딩의 자유도는 줄어든다.
- 서버사이드 렌더링

## page
- next.js의 routing을 해준다. page라는 네이밍은 반드시 지켜야 한다.

## _app.js
_app.js는 모든 Component JSX를 wrapping 해주는 파일이다. 따라서 모든 JSX를 커버링해줘야하는 작업이 있다면 이곳에 작업해주면 된다.

## Head

```
import Head from 'next/head'
```
html의 head부분을 수정할 수 있도록 해줌

## Link
```
import Link from 'next/link'
```
안에 있다.

## backend

express 사용

app.get: 가져오다
app.post: 생성하다, 애매하면 Post
app.put: 전체수정
app.delete: 제거
app.patch: 부분수정
app.options: 찔러보기 , 나 요청 보내면 응답해줄꺼야?
app.head: 헤더만 가져오기(헤더/본문)

### mysql2
node랑 mysql이랑 연결해주는 툴
sequelize: 자바스크립트로 sql를 조작해주는 라이브러리

## Issue Solution

### 1. antd shrink 할때: style을 inline으로 일일히 할 경우 responsive로 화면 사이즈 조정시 ... 로 인한 lag 발생

ℹ️ solution
styledComponent로 스타일정의한다.

### 2. antd를 styeld-components로 wrapping하여 props로 스타일 수정할 때 일반적인 방법을 사용하면 안된다.

ℹ️ solution
> react does not recognize the `backgroundcolor` prop on a dom element...

위와 같은 에러가 발생하며 [링크의 수정방법](https://stackoverflow.com/questions/62399141/react-does-not-recognize-the-backgroundcolor-prop-on-a-dom-element)과 같이 직접 props에 접근해서 css를 수정해줘야한다. 

왜 일반적인 방법이 안되는지는 [여기에 링크](https://ant.design/docs/react/faq)가 있다.

#### How do I modify Menu/Button(etc.)'s style?

> While you can override a component's style, we don't recommend doing so. antd is not only a set of React components, but also a design specification as well.