const express = require('express')
const dotenv = require('dotenv')
const path = require('path')
const hpp = require('hpp')
const helmet = require('helmet')
const app = express()
const userRouter = require('./routes/user')
const postRouter = require('./routes/post')
const indexRouter = require('./routes')
const db = require('./models')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const passportConfig = require('./passport')
const passport = require('passport')
const morgan = require('morgan')
dotenv.config()
db.sequelize
  .sync()
  .then(() => {
    console.log('db 연결성공')
  })
  .catch((e) => {
    console.log(e)
  })
passportConfig()

const sesseionProdOption = {
  resave: false,
  saveUninitialized: false,
  secret: process.env.SECRET,
  secure: true
}

const sesseionDevOption = {
  resave: false,
  saveUninitialized: false,
  secret: process.env.SECRET
}
console.log('environment:', process.env.NODE_ENV)
if (process.env.NODE_ENV === 'production') {
  app.use(morgan('combined'))
  app.use(hpp())
  app.use(helmet())
} else {
  app.use(morgan('dev'))
}

app.use(
  cors({
    origin: ['http://localhost:3000', 'https://broccolidb.com'],
    credentials: true
  })
)
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser(process.env.SECRET))
app.use(
  session(
    process.env.NODE_ENV === 'production'
      ? sesseionProdOption
      : sesseionDevOption
  )
)
app.use(passport.initialize())
app.use(passport.session())
app.use('/', express.static(path.join(__dirname, 'uploads')))
app.use('/', indexRouter)
app.use('/user', userRouter)
app.use('/post', postRouter)

app.use((err, req, res, next) => {
  if (err) {
    console.error(err)
    // throw new Error({ message: err.message, stack: err.stack })
    return res.status(500).json({ message: err.message, stack: err.stack })
  }
})
app.listen(3065, () => {
  console.log('server on 3065')
})
