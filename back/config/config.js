require('dotenv').config()

module.exports = {
  HOME:
    process.env.NODE_ENV === 'production'
      ? process.env.PROD_HOME
      : process.env.DEV_HOME,
  FRONT_HOME:
    process.env.NODE_ENV === 'production'
      ? process.env.PROD_FRONT_HOME
      : process.env.DEV_FRONT_HOME
}
