const dotenv = require('dotenv')
dotenv.config()

module.exports = {
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: 'react-faker',
    host: process.env.DB_CONNECTION,
    dialect: 'mysql'
  },
  test: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: 'react-faker',
    host: process.env.DB_CONNECTION,
    dialect: 'mysql'
  },
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: 'react-faker',
    host: process.env.DB_CONNECTION,
    dialect: 'mysql'
  }
}
