'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('users', 'lastname', {
        type: Sequelize.STRING(30),
        allowNull: false
      }),
      queryInterface.addColumn('users', 'firstname', {
        type: Sequelize.STRING(30),
        allowNull: false
      }),
      queryInterface.addColumn('users', 'birth', {
        type: Sequelize.STRING(10),
        allowNull: true
      }),
      queryInterface.addColumn('users', 'gender', {
        type: Sequelize.STRING(1),
        allowNull: true
      })
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('users', 'lastname'),
      queryInterface.removeColumn('users', 'firstname'),
      queryInterface.removeColumn('users', 'birth'),
      queryInterface.removeColumn('users', 'gender')
    ])
  }
}
