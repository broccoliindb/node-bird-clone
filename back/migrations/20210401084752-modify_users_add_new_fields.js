'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('users', 'nickname', {
        type: Sequelize.STRING(30),
        allowNull: true
      })
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('users', 'nickname')
  }
}
