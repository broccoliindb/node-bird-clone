'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('images', 'ext', {
        type: Sequelize.STRING(30),
        allowNull: false
      }),
      queryInterface.addColumn('images', 'originalname', {
        type: Sequelize.STRING(100),
        allowNull: false
      }),
      queryInterface.addColumn('images', 'author', {
        type: Sequelize.INTEGER,
        allowNull: false
      })
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('images', 'ext'),
      queryInterface.removeColumn('images', 'originalname'),
      queryInterface.removeColumn('images', 'author')
    ])
  }
}
