module.exports = (sequelize, DataTypes) => {
  const Hashtag = sequelize.define(
    'hashtag',
    {
      content: {
        type: DataTypes.STRING(140)
      }
    },
    {
      charset: 'utf8',
      collate: 'utf8_general_ci'
    }
  )
  Hashtag.associate = (db) => {
    db.Hashtag.belongsToMany(db.Post, { through: 'PostHashtag' })
  }
  return Hashtag
}
