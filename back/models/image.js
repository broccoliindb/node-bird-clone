module.exports = (sequelize, DataTypes) => {
  const Image = sequelize.define(
    'image',
    {
      src: { type: DataTypes.STRING(100), allowNull: false },
      ext: { type: DataTypes.STRING(30), allowNull: false },
      originalname: { type: DataTypes.STRING(100), allowNull: false },
      author: { type: DataTypes.INTEGER, allowNull: false }
    },
    {
      charset: 'utf8',
      collate: 'utf8_general_ci'
    }
  )
  Image.associate = (db) => {
    db.Image.belongsTo(db.Post)
  }
  return Image
}
