module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'user',
    {
      email: {
        type: DataTypes.STRING(30),
        allowNull: false,
        unique: true
      },
      password: {
        type: DataTypes.STRING(100),
        allowNull: false
      },
      lastname: {
        type: DataTypes.STRING(30),
        allowNull: false
      },
      firstname: {
        type: DataTypes.STRING(30),
        allowNull: false
      },
      birth: {
        type: DataTypes.STRING(10),
        allowNull: true
      },
      gender: {
        type: DataTypes.STRING(1),
        allowNull: true
      },
      nickname: {
        type: DataTypes.STRING(30),
        allowNull: true
      },
      phone: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    },
    {
      charset: 'utf8',
      collate: 'utf8_general_ci'
    }
  )
  User.associate = (db) => {
    db.User.hasMany(db.Post)
    db.User.hasMany(db.Comment)
    db.User.belongsToMany(db.Post, { through: 'Like', as: 'Liked' })
    db.User.belongsToMany(db.User, {
      through: 'Follow',
      as: 'Followings',
      foreignKey: 'followerId'
    })
    db.User.belongsToMany(db.User, {
      through: 'Follow',
      as: 'Followers',
      foreignKey: 'followingId'
    })
  }
  return User
}
