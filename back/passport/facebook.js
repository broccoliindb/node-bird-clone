const passport = require('passport')
const FacebookStrategy = require('passport-facebook').Strategy
const { HOME } = require('../config/config')
require('dotenv').config()

module.exports = () => {
  passport.use(
    new FacebookStrategy(
      {
        clientID: process.env.FACEBOOK_APP_ID,
        clientSecret: process.env.FACEBOOK_APP_SECRET,
        callbackURL: `${HOME}/auth/facebook/callback`,
        passReqToCallback: true,
        profileFields: ['id', 'displayName', 'photos', 'email'],
        scope: ['public_profile', 'email']
      },
      (req, accessToken, refreshToken, profile, done) => {
        console.log('req', req)
        console.log('accessToken', accessToken)
        console.log('refreshToken', refreshToken)
        console.log('profile', profile)
        return done(null, profile)
      }
    )
  )
}
