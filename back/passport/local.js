const passport = require('passport')
const { Strategy: LocalStrategy } = require('passport-local')
const { User } = require('../models')
const { compare } = require('bcrypt')

module.exports = () => {
  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password'
      },
      async (email, password, done) => {
        try {
          const user = await User.findOne({
            where: { email }
          })
          if (!user) {
            return done(null, false, { message: 'Incorrect email' })
          }
          const result = await compare(password, user.password)
          if (result) {
            return done(null, user)
          }
          return done(null, false, { message: 'Incorrect password' })
        } catch (e) {
          console.error(e)
          return done(e)
        }
      }
    )
  )
}
