const express = require('express')
const router = express.Router()
const passport = require('passport')
const { FRONT_HOME } = require('../config/config')

router.get('/', (req, res, next) => {
  res.send('home')
})

router.get(
  '/auth/facebook',
  passport.authenticate('facebook', {
    authType: 'reauthenticate',
    scope: ['email', 'public_profile']
  })
)

router.get(
  '/auth/facebook/callback',
  passport.authenticate('facebook', {
    failureRedirect: `${FRONT_HOME}/login`
  }),
  (req, res) => {
    res.redirect(`${FRONT_HOME}`)
  }
)
module.exports = router
