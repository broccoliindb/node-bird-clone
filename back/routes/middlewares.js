module.exports = {
  isLoggedIn: (req, res, next) => {
    if (req.isAuthenticated()) {
      next()
    } else {
      return res.status(401).send('로그인이 필요합니다.')
    }
  },
  isNotLoggedIn: (req, res, next) => {
    if (!req.isAuthenticated()) {
      next()
    } else {
      return res.status(401).send('이미 로그인이 하셨습니다.')
    }
  }
}
