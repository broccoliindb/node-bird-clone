const express = require('express')
const multer = require('multer')
const path = require('path')
const fs = require('fs')
const { nanoid } = require('nanoid')
const router = express.Router()
const { isLoggedIn } = require('./middlewares')
const { Post, User, Comment, Image } = require('../models')

try {
  fs.accessSync('uploads')
} catch (error) {
  console.log('no uploads folder so will be created')
  fs.mkdirSync('uploads')
}

const upload = multer({
  storage: multer.diskStorage({
    destination(req, file, done) {
      console.log('upload init')
      done(null, 'uploads')
    },
    filename(req, file, done) {
      console.log('uploadfilename', file)
      const ext = path.extname(file.originalname)
      const filename = nanoid()
      done(null, `${filename}${ext}`)
    },
    limits: { fileSize: 20 * 1024 * 1024 }
  })
})

router.patch('/:postId/like', isLoggedIn, async (req, res, next) => {
  try {
    const post = await Post.findOne({
      where: { id: parseInt(req.params.postId, 10) }
    })
    if (!post) return res.status(403).send('게시글이 존재하지 않습니다.')
    await post.addLiker(req.user.id)
    return res.json({ postId: post.id, userId: req.user.id })
  } catch (e) {
    if (e) {
      console.error(e)
      next(e)
    }
  }
})

router.patch('/:postId/dislike', isLoggedIn, async (req, res, next) => {
  try {
    const post = await Post.findOne({
      where: { id: parseInt(req.params.postId, 10) }
    })
    if (!post) return res.status(403).send('게시글이 존재하지 않습니다.')
    await post.removeLiker(req.user.id)
    return res.json({ postId: post.id, userId: req.user.id })
  } catch (e) {
    if (e) {
      console.error(e)
      next(e)
    }
  }
})

router.get('/', async (req, res, next) => {
  try {
    const posts = await Post.findAll({
      order: [['id', 'DESC']],
      include: [
        {
          model: User,
          attributes: ['id', 'nickname', 'lastname', 'firstname']
        },
        {
          model: Comment,
          include: [
            {
              model: User,
              attributes: ['id', 'nickname', 'firstname', 'lastname']
            }
          ]
        },
        { model: Image },
        { model: User, as: 'Liker', attributes: ['id'] }
      ]
    })
    return res.status(200).json(posts)
  } catch (e) {
    if (e) {
      console.error(e)
      next(e)
    }
  }
})

router.post('/:postId/comment', async (req, res, next) => {
  try {
    const post = await Post.findOne({
      where: { id: parseInt(req.params.postId, 10) }
    })
    if (!post) return res.status(403).send('게시글이 존재하지 않습니다.')
    const comment = await Comment.create({
      content: req.body.content,
      postId: parseInt(req.params.postId, 10),
      userId: req.user.id
    })
    const fullComment = await Comment.findOne({
      where: { id: comment.id },
      include: [
        {
          model: User,
          attributes: ['id', 'nickname', 'firstname', 'lastname']
        }
      ]
    })
    return res.status(201).json(fullComment)
  } catch (e) {
    if (e) {
      console.error(e)
      next(e)
    }
  }
})

router.post('/', isLoggedIn, upload.array('image'), async (req, res, next) => {
  try {
    console.log('reqfiles', req.files)
    console.log('reqbody', req.body)
    const post = await Post.create({
      content: req.body.content,
      userId: req.user.id
    })
    if (req.files) {
      const images = await Promise.all(
        req.files.map((image) => {
          const ext = path.extname(image.originalname)
          const [foldering, name] = image.path.split('/')
          return Image.create({
            src: name,
            ext: ext || image.mimetype,
            originalname: image.originalname,
            author: req.user.id,
            path: foldering
          })
        })
      )
      await post.addImages(images)
    }
    const fullPost = await Post.findOne({
      where: { id: post.id },
      order: [['id', 'DESC']],
      include: [
        {
          model: User,
          attributes: ['id', 'nickname', 'lastname', 'firstname']
        },
        {
          model: Comment,
          include: [
            {
              model: User,
              attributes: ['id', 'nickname', 'firstname', 'lastname']
            }
          ]
        },
        { model: Image, attributes: ['id', 'src', 'ext'] },
        { model: User, as: 'Liker', attributes: ['id'] }
      ]
    })
    res.status(200).json(fullPost)
    // res.status(200).send('ok')
  } catch (e) {
    if (e) {
      console.error(e)
      next(e)
    }
  }
})

module.exports = router
