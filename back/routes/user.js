const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const { User, Post } = require('../models')
const passport = require('passport')
const { isLoggedIn, isNotLoggedIn } = require('./middlewares')

router.get('/', async (req, res, next) => {
  try {
    if (req.user) {
      const userWithoutPassword = await User.findOne({
        where: { id: req.user.id },
        attributes: {
          exclude: ['password']
        },
        include: [
          { model: Post },
          { model: User, as: 'Followers' },
          { model: User, as: 'Followings' }
        ]
      })
      return res.status(200).json(userWithoutPassword)
    } else {
      return res.status(200).json(null)
    }
  } catch (error) {
    console.error(error)
    next(error)
  }
})

router.patch('/:userId', isLoggedIn, async (req, res, next) => {
  try {
    const user = await User.findOne({
      where: { id: req.user.id }
    })
    if (user) {
      await User.update(
        {
          lastname: req.body.userLastname,
          firstname: req.body.userFirstname,
          email: req.body.userEmail,
          phone: req.body.userPhone,
          nickname: req.body.userNickname
        },
        { where: { id: req.user.id } }
      )
      const modifiedUser = await User.findOne({
        where: { id: req.user.id }
      })
      res.status(200).json(modifiedUser)
    } else {
      return res.status(403).send('해당 유저 없습니다.')
    }
  } catch (error) {
    if (error) {
      console.error(error)
      next(error)
    }
  }
})

router.post('/logout', isLoggedIn, (req, res, next) => {
  req.logout()
  req.session.destroy()
  res.status(200).send('ok')
})

router.post('/login', isNotLoggedIn, (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) return next(err)
    if (info) return res.status(401).send(info)
    if (user) {
      return req.login(user, async (err) => {
        if (err) {
          console.error(err)
          return next(err)
        }
        try {
          const userWithoutPassword = await User.findOne({
            where: { id: user.id },
            attributes: {
              exclude: ['password']
            },
            include: [
              { model: Post },
              { model: User, as: 'Followers' },
              { model: User, as: 'Followings' }
            ]
          })
          return res.status(200).json(userWithoutPassword)
        } catch (e) {
          next(e)
        }
      })
    }
  })(req, res, next)
})

router.post('/signup', isNotLoggedIn, async (req, res, next) => {
  try {
    const exUser = await User.findOne({
      where: {
        email: req.body.email
      }
    })
    if (exUser) {
      return res.status(403).json({ message: '이미 사용중인 아이디입니다.' })
    }
    const hasedPasword = await bcrypt.hash(req.body.password, 12)
    await User.create({
      email: req.body.email,
      lastname: req.body.lastname,
      firstname: req.body.firstname,
      birth: req.body.birth,
      gender: req.body.gender,
      password: hasedPasword
    })
    res.status(200).send('ok')
  } catch (e) {
    console.error(e)
    next(e)
  }
})

module.exports = router
