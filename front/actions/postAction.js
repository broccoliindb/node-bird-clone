import Type from './index'

export const addPostRequestAction = (data) => {
  return {
    type: Type.ADD_POST_REQUEST,
    data
  }
}

export const addCommentRequestAction = (data) => {
  return {
    type: Type.ADD_COMMENT_REQUEST,
    data
  }
}
