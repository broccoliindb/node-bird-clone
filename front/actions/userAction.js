import Type from './index'

export const setProfileAction = (data) => {
  return {
    type: Type.CHANGE_USER_INFO_REQUEST,
    data
  }
}

export const getUserInfoAction = () => {
  return {
    type: Type.GET_USER_INFO_REQUEST
  }
}

export const loginRequestAction = (data) => {
  return {
    type: Type.LOG_IN_REQUEST,
    data
  }
}
export const logoutRequestAction = () => {
  return {
    type: Type.LOG_OUT_REQUEST
  }
}
export const signupRequestAction = (data) => {
  return {
    type: Type.SIGN_UP_REQUEST,
    data
  }
}
