import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import Header from './Header'
import { Row, Col } from 'antd'
import styled from 'styled-components'
import PostModal from './Main/PostModal'
import ErrorPage from './Error'

const Container = styled.div`
  position: relative;
  overflow-y: hidden;
  background-color: var(--baseDarkBg);
`
const Main = styled(Row)`
  height: calc(100vh - 5rem);
  overflow-y: auto;
  margin-top: 5rem;
`

const AppLayout = ({ children }) => {
  const userError = useSelector((state) => state.user.error)
  const postError = useSelector((state) => state.post.error)
  const showPostModal = useSelector((state) => state.post.showPostModal)

  useEffect(() => {
    if (userError || postError) {
      alert('에러있음')
    }
  }, [userError, postError])
  return (
    <Container>
      <Header />
      <Main>
        <Col xs={2} sm={3} md={3} lg={7} />
        <Col xs={20} sm={18} md={18} lg={10}>
          {children}
        </Col>
        <Col xs={2} sm={3} md={3} lg={7} />
      </Main>
      {showPostModal && <PostModal />}
      {/* {(userError || postError) && <ErrorPage />} */}
    </Container>
  )
}

AppLayout.propTypes = {
  children: PropTypes.node.isRequired
}

export default AppLayout
