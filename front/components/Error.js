import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const ErrorContainer = styled.div`
  background-color: red;
  width: 100vw;
  height: 100vh;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
`

const ErrorPage = ({ error }) => {
  return (
    <ErrorContainer>
      <div>ddd</div>
    </ErrorContainer>
  )
}

ErrorPage.propTypes = {
  error: PropTypes.object
}

export default ErrorPage
