import React, { useCallback, useState } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { backUrl } from '../../config/config'
const GalleryContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(24, min-content);
  grid-auto-rows: min-content;
  width: ${(props) => (props.width ? props.width : '100%')};
  max-height: ${(props) => (props.maxHeight ? props.maxHeight : '100%')};
  margin: 3px 0;
  overflow-y: auto;
  grid-gap: 5px;
`
const OverlayContainer = styled.div`
  grid-row: 1/-1;
  grid-column: 1/-1;
`
const OverlayInner = styled.div``
const ImageContainer = styled.div`
  grid-row: ${(props) => (props.rows ? props.rows : '1/-1')};
  grid-column: ${(props) => (props.columns ? props.columns : '1/-1')};
  overflow: hidden;
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
`
const Overlay = () => {
  return (
    <OverlayContainer>
      <OverlayInner>
        <button>view</button>
      </OverlayInner>
    </OverlayContainer>
  )
}

const Gallery = ({ ...props }) => {
  const [isZoom, setZoom] = useState(false)
  const { images, maxHeight, width } = props
  const setGridSpanRows = useCallback(
    (total, index) => {
      let spanRows = null
      if (total < 3) return spanRows
      if (total >= 5) {
        if (index === 0) {
          spanRows = '1/13'
        } else if (index === 1) {
          spanRows = '13/-1'
        } else if (index === 2) {
          spanRows = '1/9'
        } else if (index === 3) {
          spanRows = '9/17'
        } else if (index === 4) {
          spanRows = '17/-1'
        }
        return spanRows
      }
      switch (total) {
        case 3: {
          if (index === 0) {
            spanRows = '1/13'
          } else {
            spanRows = '13/-1'
          }
          return spanRows
        }
        case 4: {
          if (index === 1) {
            spanRows = '1/9'
          } else if (index === 2) {
            spanRows = '9/17'
          } else if (index === 3) {
            spanRows = '17/-1'
          }
          return spanRows
        }
        default:
          return spanRows
      }
    },
    [images]
  )

  const setGridSpanColumns = useCallback((total, index) => {
    let spanColumns = null
    if (total < 2) return spanColumns
    if (total >= 5) {
      if (index === 0 || index === 1) {
        spanColumns = '1/2'
      } else {
        spanColumns = '2/-1'
      }
      return spanColumns
    }
    switch (total) {
      case 2: {
        if (index === 0) {
          spanColumns = '1/2'
        } else {
          spanColumns = '2/-1'
        }
        return spanColumns
      }
      case 3: {
        if (index === 1) {
          spanColumns = '1/2'
        } else if (index === 2) {
          spanColumns = '2/-1'
        }
        return spanColumns
      }
      case 4: {
        if (index === 0) {
          spanColumns = '1/2'
        } else {
          spanColumns = '2/-1'
        }
        return spanColumns
      }
      default:
        return spanColumns
    }
  }, [])
  const onZoom = useCallback(() => {
    setZoom(true)
  }, [isZoom])
  const getImageUrl = useCallback((image) => {
    if (image.src.startsWith('blob')) {
      return image.src
    } else {
      return `${backUrl}${image.src}`
    }
  }, [])
  return (
    <GalleryContainer maxHeight={maxHeight} width={width}>
      {images.slice(0, 5).map((image, i) => (
        <ImageContainer
          key={i}
          rows={setGridSpanRows(images.length, i)}
          columns={setGridSpanColumns(images.length, i)}
        >
          <img
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'cover'
            }}
            src={getImageUrl(image)}
            role="presentation"
            onClick={onZoom}
          />
          {/* <Overlay /> */}
        </ImageContainer>
      ))}
    </GalleryContainer>
  )
}

Gallery.propTypes = {
  images: PropTypes.arrayOf(
    PropTypes.shape({
      src: PropTypes.string.isRequired
    })
  ),
  maxheight: PropTypes.string,
  width: PropTypes.string
}

export default Gallery
