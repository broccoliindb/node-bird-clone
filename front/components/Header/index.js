import React, { useMemo, useCallback, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Link from 'next/link'
import { logoutRequestAction } from '../../actions/userAction'
import { Col, Row } from 'antd'
import { status } from '../../reducers'
import {
  HomeOutlined,
  HomeFilled,
  UsergroupAddOutlined,
  VideoCameraOutlined,
  VideoCameraFilled,
  UserOutlined
} from '@ant-design/icons'
import {
  Container,
  Logo,
  TabItem,
  Tab,
  FacebookFilledWrapper,
  Form,
  Input,
  FlexBox,
  Button
} from './style'
import useHover from '../../hooks/useHover'
import { withRouter } from 'next/router'
import PropTypes from 'prop-types'
import useInput from '../../hooks/useInput'
import UserMeta from '../MetaInfo/UserMeta'
import Type from '../../actions'

const Header = ({ router }) => {
  const { pathname } = router
  const me = useSelector((state) => state.user.me)
  const dispatch = useDispatch()
  const onLogout = useCallback(() => {
    dispatch(logoutRequestAction())
  }, [])
  const antdMenuItemStyle = useMemo(
    () => ({
      fontSize: '1.5rem',
      verticalAlign: 'middle',
      margin: 0,
      padding: '0.3rem 1.5rem',
      borderRadius: '10px'
    }),
    []
  )
  const [homeRef, homeIsHover] = useHover()
  const [friendRef, friendIsHover] = useHover()
  const [videoRef, videoIsHover] = useHover()
  const [groupRef, groupIsHover] = useHover()
  const [searchTerm, onSearchTermChange] = useInput('')
  useEffect(() => {}, [pathname])
  useEffect(() => {
    dispatch({ type: Type.GET_USER_INFO_REQUEST })
  }, [])
  return (
    <Container>
      <Row>
        <Col xs={7} sm={7} md={7}>
          <FlexBox>
            <Link href="/">
              <Logo>
                <FacebookFilledWrapper />
              </Logo>
            </Link>
            <Form>
              <Input
                type="text"
                placeholder="Faker에서 검색"
                value={searchTerm}
                onChange={onSearchTermChange}
                border="var(--baseContainerDarkBg)"
                backgroundColor="var(--baseItemHoverDark)"
              ></Input>
            </Form>
          </FlexBox>
        </Col>
        <Col xs={10} sm={10} md={10}>
          <Tab>
            <Link href="/">
              <TabItem
                ref={homeRef}
                isHover={homeIsHover}
                isActive={pathname === '/'}
              >
                {pathname === '/' ? (
                  <HomeFilled style={antdMenuItemStyle} />
                ) : (
                  <HomeOutlined style={antdMenuItemStyle} />
                )}
              </TabItem>
            </Link>
            <Link href="/friend">
              <TabItem
                ref={friendRef}
                isHover={friendIsHover}
                isActive={pathname.includes('/friend')}
              >
                <UserOutlined style={antdMenuItemStyle} />
              </TabItem>
            </Link>
            <Link href="/video">
              <TabItem
                ref={videoRef}
                isHover={videoIsHover}
                isActive={pathname.includes('/video')}
              >
                {pathname.includes('/video') ? (
                  <VideoCameraFilled style={antdMenuItemStyle} />
                ) : (
                  <VideoCameraOutlined style={antdMenuItemStyle} />
                )}
              </TabItem>
            </Link>
            <Link href="/group">
              <TabItem
                ref={groupRef}
                isHover={groupIsHover}
                isActive={pathname.includes('/group')}
              >
                <UsergroupAddOutlined style={antdMenuItemStyle} />
              </TabItem>
            </Link>
          </Tab>
        </Col>
        <Col xs={7} sm={7} md={7}>
          <FlexBox justifyContent="flex-end">
            {me ? (
              <FlexBox
                justifyContent="flex-end"
                align-items="center"
                width="max-content"
              >
                <Link href="/profile">
                  <a>
                    <UserMeta />
                  </a>
                </Link>
                <Button min-width="max-content" onClick={onLogout}>
                  로그아웃
                </Button>
              </FlexBox>
            ) : (
              <Link href="/login">
                <Button width="max-content">로그인</Button>
              </Link>
            )}
          </FlexBox>
        </Col>
      </Row>
    </Container>
  )
}

Header.propTypes = {
  router: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired
}

export default withRouter(Header)
