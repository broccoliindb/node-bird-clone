import styled from 'styled-components'
import { FacebookFilled } from '@ant-design/icons'
import { FlexBox, Input, Button } from '../../rootStyles'

export const Container = styled.div`
  padding: 0.5rem 1rem 0 1rem;
  background-color: var(--baseContainerDarkBg);
  position: fixed;
  width: 100%;
  border-bottom: 1px solid var(--baseBorderDarkGrey);
`
export const Tab = styled.ul`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: var(--baseContainerDarkBg);
  border-bottom: 1px solid var(--baseContainerDarkBg);
  height: 100%;
`
export const TabItem = styled.li`
  margin: 0 0.2rem 0;
  width: 100%;
  text-align: center;
  color: var(--baseFontGrey);
  cursor: pointer;
  font-size: 1.5rem;
  padding: 0.2rem 0.2rem 0.4rem 0.2rem;
  border-bottom: 3px solid
    ${(props) => (props.isActive ? 'var(--baseActive)' : 'inherit')};
  color: ${(props) =>
    props.isActive ? 'var(--baseActive)' : 'var(--baseFontGrey)'};
  span {
    background-color: ${(props) =>
      props.isHover && !props.isActive
        ? 'var(--baseItemHoverDark)'
        : 'transperant'};
  }
`
export const Logo = styled.div`
  width: 2.2rem;
  height: 2.2rem;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0;
  margin-right: 1rem;
  cursor: pointer;
  background-color: white;
`
export const FacebookFilledWrapper = styled(FacebookFilled)`
  color: var(--baseLogoBlue);
  font-size: 2.6rem;
`
export const Form = styled.form`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`
export { FlexBox, Input, Button }
