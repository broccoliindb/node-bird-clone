import React, { useState, useCallback, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import SignUp from '../SignUp'
import { loginRequestAction } from '../../actions/userAction'
import { useRouter } from 'next/router'
import { status } from '../../reducers'
import { backUrl } from '../../config/config'
import TYPES from '../../actions'
import {
  Container,
  Title,
  Text,
  Input,
  LoginContainer,
  Form,
  FlexBoxWithBottomBorder,
  ButtonContainer,
  Button
} from './style'
import ErrorPage from '../Error'

const Login = () => {
  const { loading, statusMessage } = useSelector((state) => state.user)
  const dispatch = useDispatch()
  const router = useRouter()
  const me = useSelector((state) => state.user.me)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [shouldOpenSignUp, setOpenSignUp] = useState(false)
  const loginSubmitted = useCallback(
    (e) => {
      e.preventDefault()
      dispatch(loginRequestAction({ email, password }))
    },
    [email, password]
  )
  const onLoginFacebook = useCallback(() => {
    // window.location.href = `${window.location.origin}/auth/facebook`
    // dispatch({ type: TYPES.FACEBOOK_LOGIN_REQUEST })
  }, [])
  const onChangeId = useCallback((e) => {
    setEmail(e.target.value)
  }, [])
  const onChangePassword = useCallback((e) => {
    setPassword(e.target.value)
  }, [])
  const onOpenSignUp = useCallback((e) => {
    setOpenSignUp(true)
  }, [])

  useEffect(() => {
    if (me) {
      router.replace('/')
    }
  }, [me])
  useEffect(() => {
    if (loading && loading !== status.loading) {
      if (statusMessage) alert(statusMessage)
      dispatch({ type: TYPES.INIT_LOADING_STATUS })
    }
  }, [loading])
  return (
    <Container>
      <Title>fackerbook</Title>
      <LoginContainer>
        <Form onSubmit={loginSubmitted}>
          <Input
            placeholder="이메일"
            type="text"
            value={email}
            onChange={onChangeId}
            required
          />
          <Input
            placeholder="비밀번호"
            type="password"
            value={password}
            onChange={onChangePassword}
            required
          />
          <Button htmlType="submit">로그인</Button>
        </Form>
        <ButtonContainer>
          <FlexBoxWithBottomBorder isHoverable={true} justifyContent="center">
            <Text>비밀번호를 잊으셨나요?</Text>
          </FlexBoxWithBottomBorder>
          <a href={`${backUrl}auth/facebook`}>
            <Button>facebook으로 로그인</Button>
          </a>
          <Button
            style={{ backgroundColor: 'var(--baseGreen)' }}
            onClick={onOpenSignUp}
          >
            새 계정 만들기
          </Button>
        </ButtonContainer>
      </LoginContainer>
      {shouldOpenSignUp && <SignUp setOpenSignUp={setOpenSignUp} />}
      {/* {userError && <ErrorPage />} */}
    </Container>
  )
}

export default Login
