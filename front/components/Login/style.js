import styled from 'styled-components'
import {
  Button,
  Input,
  FlexBoxWithBottomBorder,
  Title,
  Text
} from '../../rootStyles'

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
  background-color: var(--baseContainerLightBg);
`
export const LoginContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 1rem;
  margin: 1.5rem;
  border-radius: 8px;
  background-color: var(--baseWhite);
  box-shadow: 0 2px 4px rgb(0 0 0 / 10%), 0 8px 16px rgb(0 0 0 / 10%);
`
export const Form = styled.form`
  min-width: 20rem;
  max-width: 26rem;
  display: grid;
  grid-gap: 0.8rem;
  margin: 0.3rem 0;
  grid-template-rows: repeat(3, 2.5rem);
`
export const ButtonContainer = styled.div`
  display: grid;
  grid-gap: 0.8rem;
  width: 100%;
  grid-template-rows: repeat(3, 2.5rem);
`
export { Button, Input, FlexBoxWithBottomBorder, Title, Text }
