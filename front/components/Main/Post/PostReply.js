import React from 'react'
import { List } from 'antd'
import PropTypes from 'prop-types'
import { AvatarWrapper, ListItemMetaWrapper } from '../../../rootStyles'
import { UserOutlined } from '@ant-design/icons'
import ReplyForm from '../../ReplyForm'

const PostReply = ({ comments, postId }) => {
  console.log(comments)
  return (
    <List
      style={{ border: '1px solid red' }}
      dataSource={comments}
      renderItem={(item) => (
        <List.Item key={item.id}>
          <ListItemMetaWrapper
            avatar={<AvatarWrapper icon={<UserOutlined />} />}
            title={item.user.nickname || item.user.firstname}
            description={item.content}
          ></ListItemMetaWrapper>
        </List.Item>
      )}
    >
      <ReplyForm postId={postId} />
    </List>
  )
}

PostReply.propTypes = {
  postId: PropTypes.string.isRequired,
  comments: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      user: PropTypes.shape({
        nickname: PropTypes.string.isRequired,
        firstname: PropTypes.string.isRequired,
        lastname: PropTypes.string.isRequied
      }).isRequired,
      content: PropTypes.string.isRequired
    })
  )
}

export default PostReply
