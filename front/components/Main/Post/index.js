import React, { useCallback, useState, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import Type from '../../../actions'
import {
  EllipsisOutlined,
  LikeOutlined,
  CommentOutlined,
  UserOutlined,
  LikeFilled
} from '@ant-design/icons'
import {
  ContentContainer,
  CardContainer,
  AvatarWrapper,
  Text,
  FlexBox,
  Button
} from '../../../rootStyles'
import Gallery from '../../Gallery'
import PostReply from './PostReply'

const Post = ({ post }) => {
  const dispatch = useDispatch()
  const me = useSelector((state) => state.user.me)
  const [isLike, setLike] = useState(false)
  const [showComments, setShowComments] = useState(false)
  const onCommentClick = useCallback(() => {
    setShowComments((prev) => !prev)
  }, [showComments])
  const onClickLike = useCallback(
    (isLike) => () => {
      if (!isLike) {
        dispatch({ type: Type.LIKE_POST_REQUEST, data: post.id })
      } else {
        dispatch({ type: Type.DISLIKE_POST_REQUEST, data: post.id })
      }
    },
    []
  )
  useEffect(() => {
    setLike(post.Liker.find((i) => i.id === (me && me.id)))
  }, [post, me])
  return (
    <ContentContainer>
      <CardContainer
        title={
          <FlexBox width="max-content">
            <AvatarWrapper margin="0 0.5rem 0 0" icon={<UserOutlined />} />
            <Text width="max-content" fontSize="1.5rem">
              {post.user.nickname || post.user.lastname}
            </Text>
          </FlexBox>
        }
        extra={
          <EllipsisOutlined
            style={{ fontSize: '2rem', color: 'var(--baseFontGrey)' }}
          />
        }
        actions={[
          <Button
            key="like"
            backgroundColor="var(--baseContainerDarkBg)"
            fontSize="1.1rem"
            padding="0.3rem 1rem"
            color="var(--baseFontGrey)"
            onClick={onClickLike(isLike)}
          >
            {isLike ? (
              <LikeFilled
                style={{ marginRight: '0.5rem', color: 'var(--baseBlue)' }}
              />
            ) : (
              <LikeOutlined style={{ marginRight: '0.5rem' }} />
            )}
            {isLike ? (
              <span style={{ color: 'var(--baseBlue)' }}>좋아요</span>
            ) : (
              <span>좋아요</span>
            )}
          </Button>,
          <Button
            key="comment"
            backgroundColor="var(--baseContainerDarkBg)"
            fontSize="1.1rem"
            padding="0.3rem 1rem"
            color="var(--baseFontGrey)"
            onClick={onCommentClick}
          >
            <CommentOutlined style={{ marginRight: '0.5rem' }} />
            댓글 달기
          </Button>
        ]}
      >
        <FlexBox
          flexDirection="column"
          justifyContent="left"
          alignItems="flex-start"
          padding="0 0 1rem 0"
          style={{ borderBottom: '1px solid var(--baseBorderDarkGrey)' }}
        >
          <Text textAlign="left">{post.content}</Text>
          <Gallery images={post.images} />
        </FlexBox>
        <FlexBox>
          <Text
            onClick={onCommentClick}
            textAlign="right"
            style={{ cursor: 'pointer' }}
          >{`댓글 ${post.comments.length}개`}</Text>
        </FlexBox>
      </CardContainer>
      <div
        style={{
          borderTop: '1px solid var(--baseBorderDarkGrey)',
          padding: '1rem 0 0 0'
        }}
      >
        {showComments && (
          <PostReply comments={post.comments} postId={post.id} />
        )}
      </div>
    </ContentContainer>
  )
}

Post.propTypes = {
  post: PropTypes.shape({
    comments: PropTypes.arrayOf(
      PropTypes.shape({
        user: PropTypes.object.isRequired,
        content: PropTypes.string.isRequired
      })
    ),
    images: PropTypes.arrayOf(
      PropTypes.shape({
        src: PropTypes.string.isRequired
      })
    ),
    user: PropTypes.shape({
      id: PropTypes.number.isRequired,
      nickname: PropTypes.string,
      lastname: PropTypes.string
    }),
    content: PropTypes.string,
    id: PropTypes.number.isRequired,
    Liker: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired
      })
    )
  }).isRequired
}

export default Post
