import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Types from '../../actions'
import { UserOutlined } from '@ant-design/icons'
import {
  ContentContainer,
  Input,
  FlexBox,
  AvatarWrapper
} from '../../rootStyles'

const PostForm = () => {
  const dispatch = useDispatch()
  const { me, randomColor } = useSelector((state) => state.user)
  const { loading } = useSelector((state) => {
    return state.user
  })
  const onInputClick = useCallback(
    (me) => (e) => {
      if (me) {
        dispatch({ type: Types.POST_MODAL_ON })
      }
    },
    []
  )
  return (
    <ContentContainer>
      <FlexBox>
        <AvatarWrapper
          backgroundColor={randomColor}
          size={36}
          icon={<UserOutlined />}
          margin="0 0.5rem 0 0"
        />
        <Input
          onClick={onInputClick(me)}
          placeholder="무슨생각을 하고 있나요?"
          readOnly
          border="var(--baseContainerDarkBg)"
          backgroundColor="var(--baseItemHoverDark)"
        ></Input>
      </FlexBox>
    </ContentContainer>
  )
}

export default PostForm
