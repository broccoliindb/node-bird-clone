import React from 'react'
import styled from 'styled-components'
import PostModalContent from './PostModalContent'

const Container = styled.div`
  background-color: rgba(0, 0, 0, 0.5);
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 1;
  top: 0;
  left: 0;
`
const Inner = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-bottom: 10rem;
  height: 100vh;
`
const PostModal = () => {
  return (
    <Container>
      <Inner>
        <PostModalContent />
      </Inner>
    </Container>
  )
}

export default PostModal
