import React, { useCallback, useRef, useEffect, useState } from 'react'
import { PictureOutlined } from '@ant-design/icons'
import styled from 'styled-components'
import Types from '../../actions'
import { useDispatch, useSelector } from 'react-redux'
import {
  Title,
  Close,
  CardContainer,
  Form,
  Button,
  Textarea,
  FlexBox,
  Text
} from '../../rootStyles'
import UserMeta from '../MetaInfo/UserMeta'
import { addPostRequestAction } from '../../actions/postAction'
import useInput from '../../hooks/useInput'
import faker from 'faker'
import Gallery from '../Gallery'

const Container = styled.div`
  border: 2px solid var(--baseBorderDarkGrey);
  border-radius: 8px;
`

const PostFormContent = () => {
  const [files, setFiles] = useState([])
  const postRef = useRef(null)
  const dispatch = useDispatch()
  const { me } = useSelector((state) => state.user)
  const onCloseModal = useCallback(() => {
    dispatch({ type: Types.POST_MODAL_OFF })
  }, [])
  const [postContent, onChangePostContent] = useInput('')
  const onAddPost = useCallback(
    (e) => {
      e.preventDefault()
      const formData = new FormData()
      formData.append('content', postContent)
      files.forEach((file) => formData.append('image', file.originFile))
      console.log('onAddPost', formData)
      dispatch(addPostRequestAction(formData))
      setFiles([])
    },
    [files, postContent]
  )
  useEffect(() => {
    postRef.current.focus()
  }, [])
  const onChooseFile = useCallback((e) => {
    const files = []
    if (e.target.files.length === 0) return
    for (let i = 0; i < e.target.files.length; i++) {
      files.push({
        src: URL.createObjectURL(e.target.files[i]),
        originFile: e.target.files[i]
      })
    }
    setFiles(files)
  }, [])
  const onRemoveImages = useCallback(() => {
    setFiles([])
  }, [])
  return (
    <Container>
      <CardContainer
        padding="0 1rem"
        title={
          <Title fontSize="1.5rem" color="var(--baseWhite)">
            게시물 만들기
          </Title>
        }
        extra={<Close onClick={onCloseModal} />}
        actions={[
          <Button onClick={onAddPost} key="button">
            게시
          </Button>
        ]}
      >
        <UserMeta />
        <Form onSubmit={onAddPost} encType="multipart/form-data">
          <Textarea
            ref={postRef}
            rows={3}
            cols={50}
            placeholder="무슨생각을 하고 있나요?"
            value={postContent || ''}
            onChange={onChangePostContent}
          ></Textarea>
          <FlexBox
            style={{
              border: '2px solid var(--baseBorderDarkGrey)',
              borderRadius: '8px',
              marginBottom: '1rem'
            }}
          >
            {files.length > 0 && (
              <Close
                style={{ margin: '1rem 2rem 0 0' }}
                onClick={onRemoveImages}
              />
            )}
            <Gallery images={files} maxHeight="15rem" width="43rem" />
          </FlexBox>
          <FlexBox
            style={{
              border: '1px solid grey',
              borderRadius: '8px'
            }}
            justifyContent="space-between"
            padding="0.5rem 1rem"
          >
            <Text isBold={true} color="var(--baseWhite)" width="max-content">
              게시물에 추가
            </Text>
            <input
              type="file"
              id="file"
              name="image"
              multiple
              onChange={onChooseFile}
            ></input>
            <label htmlFor="file">
              <PictureOutlined
                key="picture"
                style={{ color: 'var(--baseYellowGreen)', fontSize: '2rem' }}
              />
            </label>
          </FlexBox>
        </Form>
      </CardContainer>
    </Container>
  )
}

export default PostFormContent
