import React, { useEffect } from 'react'
import PostForm from '../Main/PostForm'
import Post from '../Main/Post'
import { useSelector, useDispatch } from 'react-redux'
import Type from '../../actions'
const Main = () => {
  const { me } = useSelector((state) => state.user)
  const mainPosts = useSelector((state) => state.post.mainPosts)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch({ type: Type.GET_POST_LIST_REQUEST })
  }, [])
  return (
    <>
      {me && <PostForm />}
      {mainPosts.map((post) => (
        <Post key={post.id} post={post} />
      ))}
    </>
  )
}

export default Main
