import React from 'react'
import { Text } from '../../rootStyles'
import { useSelector } from 'react-redux'

const Meta = () => {
  const { nickname, firstname, lastname } = useSelector(
    (state) => state.user.me
  )
  return (
    <Text color="var(--baseWhite)" width="max-content">
      {nickname || `${firstname} ${lastname}`}
    </Text>
  )
}

export default Meta
