import React from 'react'
import { FlexBox, AvatarWrapper } from '../../rootStyles'
import Meta from './Meta'
import { useSelector } from 'react-redux'
import { UserOutlined } from '@ant-design/icons'

const UserMeta = () => {
  const { me, randomColor } = useSelector((state) => state.user)
  return (
    <FlexBox>
      <AvatarWrapper
        backgroundColor={randomColor}
        size={36}
        icon={<UserOutlined />}
        margin="0 0.5rem 0 0"
      />
      <FlexBox
        flexDirection="column"
        justifyContent="center"
        alignItems="flex-start"
        width="max-content"
        margin="0 1rem 0 0"
      >
        <Meta />
      </FlexBox>
    </FlexBox>
  )
}

export default UserMeta
