import React, { useCallback, useRef } from 'react'
import styled from 'styled-components'
import { AvatarWrapper, Input, Button } from '../../rootStyles'
import { UserOutlined } from '@ant-design/icons'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import useInput from '../../hooks/useInput'
import { addCommentRequestAction } from '../../actions/postAction'

const Container = styled.div`
  color: var(--baseFontGrey);
`
const ReplyForm = ({ postId }) => {
  const { me } = useSelector((state) => state.user)
  const dispatch = useDispatch()
  const [comment, onChangeComment] = useInput('')
  const refForm = useRef(null)
  const onSubmitHandler = useCallback(
    (e) => {
      e.preventDefault()
      dispatch(addCommentRequestAction({ content: comment, postId }))
    },
    [comment]
  )
  return (
    <Container>
      {me && (
        <>
          <span>댓글 1개 더보기</span>
          <div style={{ display: 'flex', width: '100%', marginTop: '1rem' }}>
            <AvatarWrapper margin=" 0 1rem 0 0" icon={<UserOutlined />} />
            <form
              ref={refForm}
              style={{ width: '100%' }}
              onSubmit={onSubmitHandler}
            >
              <Input
                type="text"
                placeholder="댓글 작성..."
                border="var(--baseContainerDarkBg)"
                backgroundColor="var(--baseItemHoverDark)"
                value={comment || ''}
                onChange={onChangeComment}
              />
            </form>
          </div>
        </>
      )}
    </Container>
  )
}

ReplyForm.propTypes = {
  postId: PropTypes.number.isRequired
}
export default ReplyForm
