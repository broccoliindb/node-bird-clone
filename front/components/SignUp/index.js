import React, { useCallback, useState, useEffect } from 'react'
import { DatePicker, Radio } from 'antd'
import PropTypes from 'prop-types'
import useInput from '../../hooks/useInput'
import { useDispatch, useSelector } from 'react-redux'
import { signupRequestAction } from '../../actions/userAction'
import { status } from '../../reducers'
import {
  Container,
  SignUpForm,
  Close,
  FlexBox,
  Input,
  Button,
  Title,
  FlexBoxWithBottomBorder
} from './style'
import { Text } from '../../rootStyles'
import TYPES from '../../actions'

const SignUp = ({ setOpenSignUp }) => {
  const dispatch = useDispatch()
  const [lastname, onChangedLastName] = useInput('')
  const [firstname, onChangedFirstName] = useInput('')
  const [email, onChangedEmail] = useInput('')
  const [password, onChangePassword] = useInput('')
  const [confirmPassword, setConfirmPasword] = useState('')
  const [gender, onChangeGender] = useInput('')
  const [passwordInfo, setPasswordInfo] = useState('')
  const [birth, setBirth] = useState(null)
  const { loading, statusMessage } = useSelector((state) => state.user)
  const checkValidation = useCallback((pw, pw2) => {
    let valid = true
    if (pw !== pw2) {
      valid = false
    }
    return valid
  }, [])
  const onChangeConfirmPassword = useCallback(
    (e) => {
      setConfirmPasword(e.target.value)
      if (e.target.value && !checkValidation(password, e.target.value)) {
        setPasswordInfo('비밀번호가 일치하지 않습니다.')
      } else {
        setPasswordInfo('')
      }
    },
    [password]
  )

  const onSignUp = useCallback(
    (e) => {
      e.preventDefault()
      if (!checkValidation(password, confirmPassword)) return
      dispatch(
        signupRequestAction({
          lastname,
          firstname,
          email,
          password,
          birth,
          gender
        })
      )
    },
    [lastname, firstname, email, password, birth, gender, confirmPassword]
  )
  const onCloseSignUp = useCallback(() => {
    setOpenSignUp(false)
  }, [])
  const onChangeBirth = useCallback(
    (birth, birthString) => {
      setBirth(birthString)
    },
    [birth]
  )
  useEffect(() => {
    if (loading && loading !== status.loading) {
      alert(statusMessage)
      if (loading === status.done) {
        setOpenSignUp(false)
      }
      dispatch({ type: TYPES.INIT_LOADING_STATUS })
    }
  }, [loading])
  return (
    <Container>
      <SignUpForm onSubmit={onSignUp} autoComplete="off">
        <Close onClick={onCloseSignUp} />
        <Title
          color="var(--baseBlack)"
          fontSize="2rem"
          textAlign="left"
          margin="0 0 1rem 0"
        >
          가입하기
        </Title>
        <FlexBoxWithBottomBorder
          justifyContent="flex-start"
          margin="0 0 1rem 0"
          color="var(--baseBlack)"
          padding="0 0 1rem 0"
          isOpactity={true}
        >
          빠르고 쉽습니다.
        </FlexBoxWithBottomBorder>
        <FlexBox justifyContent="space-between">
          <Input
            type="text"
            placeholder="성"
            value={lastname || ''}
            onChange={onChangedLastName}
            margin="0 0.5rem 0.5rem 0"
            required
          />
          <Input
            type="text"
            placeholder="이름"
            value={firstname || ''}
            onChange={onChangedFirstName}
            margin="0 0 0.5rem 0"
            required
          />
        </FlexBox>
        <Input
          type="email"
          placeholder="email"
          value={email || ''}
          onChange={onChangedEmail}
          margin="0 0 0.5rem 0"
          required
          autoComplete="email"
        />
        <Input
          type="password"
          placeholder="비밀번호"
          value={password || ''}
          onChange={onChangePassword}
          margin="0 0 0.5rem 0"
          required
        />
        <Input
          type="password"
          placeholder="비밀번호확인"
          value={confirmPassword || ''}
          onChange={onChangeConfirmPassword}
          margin="0 0 0.5rem 0"
          required
        />
        {passwordInfo && (
          <Text textAlign="left" margin="0 0 0.5rem 0" color="red">
            {passwordInfo}
          </Text>
        )}
        <FlexBox>
          <FlexBox width="max-content" margin="0 1rem 0 0">
            <div>생일</div>
            <DatePicker
              onChange={onChangeBirth}
              format="YYYY-MM-DD"
              allowClear={false}
              style={{ marginLeft: '0.5rem' }}
            />
          </FlexBox>
          <Radio.Group onChange={onChangeGender} value={gender}>
            <Radio value="F">여성</Radio>
            <Radio value="M">남성</Radio>
          </Radio.Group>
        </FlexBox>
        <Button
          padding="0.8rem"
          margin="2rem 0 0 0"
          backgroundColor="var(--baseGreen)"
          htmlType="submit"
        >
          가입하기
        </Button>
      </SignUpForm>
    </Container>
  )
}

SignUp.propTypes = {
  setOpenSignUp: PropTypes.func.isRequired
}

export default SignUp
