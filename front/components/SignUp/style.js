import styled from 'styled-components'
import { CloseOutlined } from '@ant-design/icons'
import {
  FlexBox,
  Input,
  Button,
  Title,
  FlexBoxWithBottomBorder
} from '../../rootStyles'
export const Container = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(255, 255, 255, 0.5);
`
export const SignUpForm = styled.form`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 25rem;
  padding: 2rem;
  border-radius: 8px;
  background-color: var(--baseWhite);
  box-shadow: 0 2px 4px rgb(0 0 0 / 10%), 0 8px 16px rgb(0 0 0 / 10%);
`
export const Close = styled(CloseOutlined)`
  position: absolute;
  right: 0;
  top: 0;
  padding: 0.5rem;
  font-size: 1.2rem;
`
export { FlexBox, Input, Button, Title, FlexBoxWithBottomBorder }
