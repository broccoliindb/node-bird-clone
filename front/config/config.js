export const backUrl =
  process.env.NODE_ENV === 'production'
    ? 'https://api.broccolidb.com/'
    : 'http://localhost:3065/'
