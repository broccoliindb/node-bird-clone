import { useState, useRef, useEffect } from 'react'
const useHover = () => {
  const [value, setValue] = useState(false)
  const ref = useRef(null)
  const enterHandler = (e) => {
    setValue(true)
  }
  const leaveHandler = (e) => {
    setValue(false)
  }
  useEffect(() => {
    const node = ref.current
    if (node) {
      node.addEventListener('mouseenter', enterHandler)
      node.addEventListener('mouseleave', leaveHandler)
      return () => {
        node.removeEventListener('mouseenter', enterHandler)
        node.removeEventListener('mouseleave', leaveHandler)
      }
    }
  }, [value])
  return [ref, value]
}

export default useHover
