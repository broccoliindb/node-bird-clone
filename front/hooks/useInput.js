import { useState, useCallback } from 'react'

const useInput = ({ inintialValue }) => {
  const [value, setter] = useState(inintialValue)
  const handler = useCallback((e) => {
    setter(e.target.value)
  }, [])
  return [value, handler, setter]
}

export default useInput
