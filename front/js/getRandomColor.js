const getRandomColor = () => {
  return `#${Math.floor(Math.random() * parseInt('ffffff', 16)).toString(16)}`
}
export default getRandomColor
