import React from 'react'
import PropTypes from 'prop-types'
import { createGlobalStyle } from 'styled-components'
import 'antd/dist/antd.css'
import reset from 'styled-reset'
import Head from 'next/head'
import wrapper from '../store/configureStore'
import withReduxSaga from 'next-redux-saga'
import '../rootVariables.css'
import { useSelector } from 'react-redux'
const GlobalStyle = createGlobalStyle`
${reset};
* {
  box-sizing: border-box;
}
`

const App = ({ Component }) => {
  const { loading, statusMessage } = useSelector((state) => state.user)
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <title>Node Bird</title>
      </Head>
      <Component />
      <GlobalStyle />
    </>
  )
}

App.propTypes = {
  Component: PropTypes.elementType.isRequired
}

export default wrapper.withRedux(withReduxSaga(App))
