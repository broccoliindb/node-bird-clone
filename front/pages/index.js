import React from 'react'
import AppLayout from '../components/AppLayout'
import Main from '../components/Main'
const Home = () => {
  return (
    <AppLayout>
      <Main />
    </AppLayout>
  )
}

export default Home
