import React, { useEffect } from 'react'
import AppLayout from '../components/AppLayout'
import styled from 'styled-components'
import { useSelector, useDispatch } from 'react-redux'
import useInput from '../hooks/useInput'
import { Form, Input, Button } from '../rootStyles'
import { setProfileAction } from '../actions/userAction'
const Container = styled.div`
  background-color: red;
`

const Profile = () => {
  const dispatch = useDispatch()
  const onClickSubmit = (e) => {
    e.preventDefault()
    dispatch(
      setProfileAction({
        userLastname,
        userFirstname,
        userPhone,
        userEmail,
        userNickname,
        id: me.id
      })
    )
  }
  const me = useSelector((state) => state.user.me)
  const [userLastname, onChangeLastname, setLastname] = useInput('')
  const [userFirstname, onChangefirstName, setFirstname] = useInput('')
  const [userPhone, onChangePhone, setPhone] = useInput('')
  const [userEmail, onChangeEmail, setEmail] = useInput('')
  const [userNickname, onChangeNickname, setNickname] = useInput('')
  useEffect(() => {
    if (me && me.lastname) {
      setLastname(me.lastname)
    }
    if (me && me.firstname) {
      setFirstname(me.firstname)
    }
    if (me && me.email) {
      setEmail(me.email)
    }
    if (me && me.phone) {
      setPhone(me.phone)
    }
    if (me && me.nickname) {
      setNickname(me.nickname)
    }
  }, [me])
  return (
    <AppLayout>
      <Container>
        {/* <Avatar /> */}
        {/* <Text /> */}
        <Form onSubmit={onClickSubmit}>
          <label htmlFor="lastname">성</label>
          <Input
            id="lastname"
            type="text"
            value={userLastname}
            onChange={onChangeLastname}
            placeholder="성"
          />
          <Input
            type="text"
            value={userFirstname}
            onChange={onChangefirstName}
            placeholder="이름"
          />
          <Input
            type="number"
            value={userPhone}
            onChange={onChangePhone}
            placeholder="핸드폰번호는 -없이 숫자만 입력해주세요"
          />
          <Input
            type="email"
            value={userEmail}
            onChange={onChangeEmail}
            placeholder="이메일"
          />
          <Input
            type="text"
            value={userNickname}
            onChange={onChangeNickname}
            placeholder="별명"
          />
          <Button type="submit">수정</Button>
        </Form>
      </Container>
    </AppLayout>
  )
}

export default Profile
