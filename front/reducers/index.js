import { HYDRATE } from 'next-redux-wrapper'
import { combineReducers } from 'redux'
import post from './post'
import user from './user'

export const status = {
  loading: 'loading',
  error: 'error',
  done: 'done'
}

const rootReducer = combineReducers({
  index: (state = {}, action) => {
    switch (action.type) {
      case HYDRATE:
        return { ...state, ...action.payload }
      default:
        return state
    }
  },
  user,
  post
})

export default rootReducer
