import produce from 'immer'
import Types from '../actions'
import { nanoid } from 'nanoid'
import { status } from '../reducers'
export const postInitStae = {
  loading: null,
  statusMessage: null,
  error: null,
  mainPosts: [
    // {
    //   id: nanoid(),
    //   User: {
    //     id: nanoid(),
    //     nickname: 'cyb'
    //   },
    //   content: '첫번째 게시글 #해시',
    //   Images: [
    //     { src: 'https://fakeimg.pl/300/' },
    //     { src: 'https://fakeimg.pl/350x200/ff0000,128/000,255' }
    //   ],
    //   Comments: [
    //     {
    //       id: nanoid(),
    //       User: {
    //         id: nanoid(),
    //         nickname: 'cyb1'
    //       },
    //       content: '테스트글'
    //     },
    //     {
    //       id: nanoid(),
    //       User: {
    //         id: nanoid(),
    //         nickname: 'cyb2'
    //       },
    //       content: '테스트글2'
    //     }
    //   ]
    // },
    // {
    //   id: nanoid(),
    //   User: {
    //     id: nanoid(),
    //     nickname: '브로콜리'
    //   },
    //   content: '두번째 게시글 #해시',
    //   Images: [
    //     { src: 'https://fakeimg.pl/300/' },
    //     { src: 'https://fakeimg.pl/350x200/ff0000,128/000,255' },
    //     { src: 'https://fakeimg.pl/250x100/ff0000/' }
    //   ],
    //   Comments: [
    //     {
    //       id: nanoid(),
    //       User: {
    //         nickname: '브로콜리'
    //       },
    //       content: '테스트글'
    //     },
    //     {
    //       id: nanoid(),
    //       User: {
    //         nickname: '브로콜리'
    //       },
    //       content: '테스트글2'
    //     }
    //   ]
    // }
  ],
  imagePaths: []
}

const reducer = (prevState = postInitStae, action) => {
  return produce(prevState, (draft) => {
    switch (action.type) {
      case Types.POST_MODAL_OFF: {
        draft.showPostModal = false
        break
      }
      case Types.POST_MODAL_ON: {
        draft.showPostModal = true
        break
      }

      case Types.ADD_POST_FULFILLED: {
        draft.loading = status.done
        draft.mainPosts = [action.payload, ...draft.mainPosts]
        break
      }
      case Types.ADD_COMMENT_FULFILLED: {
        draft.loading = status.done
        const post = draft.mainPosts.find((i) => i.id === action.payload.postId)
        post.comments.unshift(action.payload)
        break
      }
      case Types.GET_POST_LIST_FULFILLED: {
        draft.loading = status.done
        draft.mainPosts = action.payload
        break
      }
      case Types.LIKE_POST_FULFILLED: {
        draft.loading = status.done
        const post = draft.mainPosts.find((i) => i.id === action.payload.postId)
        post.Liker.push({ id: action.payload.userId })
        break
      }
      case Types.DISLIKE_POST_FULFILLED: {
        draft.loading = status.done
        const post = draft.mainPosts.find((i) => i.id === action.payload.postId)
        post.Liker = post.Liker.filter((i) => i.id !== action.payload.userId)
        break
      }
      case Types.DISLIKE_POST_REQUEST:
      case Types.LIKE_POST_REQUEST:
      case Types.ADD_POST_REQUEST:
      case Types.GET_POST_LIST_REQUEST:
      case Types.ADD_COMMENT_REQUEST: {
        draft.loading = status.loading
        break
      }
      case Types.DISLIKE_POST_REJECTED:
      case Types.ADD_POST_REJECTED:
      case Types.GET_POST_LIST_REJECTED:
      case Types.ADD_COMMENT_REJECTED: {
        draft.loading = status.error
        draft.statusMessage = action.error.message || '문제가 발생했습니다.'
        draft.error = action.error.stack
        break
      }
      default:
        break
    }
  })
}

export default reducer
