import produce from 'immer'
import Types from '../actions'
import getRandomColor from '../js/getRandomColor'
import { status } from '../reducers'

export const userInitState = {
  loading: null,
  statusMessage: null,
  error: null,
  me: null,
  signUpdata: {}
}

const reducer = (prevState = userInitState, action) => {
  return produce(prevState, (draft) => {
    switch (action.type) {
      case Types.INIT_LOADING_STATUS: {
        draft.loading = null
        draft.statusMessage = null
        draft.error = null
        break
      }
      case Types.CHANGE_USER_INFO_FULFILLED: {
        draft.loading = status.done
        draft.me = action.payload
        break
      }
      case Types.GET_USER_INFO_FULFILLED: {
        draft.loading = status.done
        draft.me = action.payload
        break
      }
      case Types.SIGN_UP_FULFILLED: {
        draft.loading = status.done
        draft.statusMessage = action.message
        break
      }
      case Types.LOG_IN_FULFILLED: {
        draft.me = action.payload
        draft.randomColor = getRandomColor()
        draft.loading = status.done
        break
      }
      case Types.LOG_OUT_FULFILLED: {
        draft.loading = status.done
        draft.me = null
        break
      }
      case Types.CHANGE_USER_INFO_REJECTED:
      case Types.GET_USER_INFO_REJECTED:
      case Types.SIGN_UP_REJECTED:
      case Types.LOG_OUT_REJECTED:
      case Types.LOG_IN_REJECTED: {
        draft.loading = status.error
        draft.statusMessage = action.error.message || '문제가 발생했습니다.'
        draft.error = action.error.stack
        break
      }
      case Types.CHANGE_USER_INFO_REQUEST:
      case Types.LOG_IN_REQUEST:
      case Types.GET_USER_INFO_REQUEST:
      case Types.SIGN_UP_REQUEST:
      case Types.LOG_OUT_REQUEST: {
        draft.loading = status.loading
        break
      }
      default:
        break
    }
  })
}

export default reducer
