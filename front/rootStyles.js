import React from 'react'
import styled, { css } from 'styled-components'
import { CloseOutlined } from '@ant-design/icons'
import { Card, Avatar, List } from 'antd'

export const Input = styled.input`
  border-radius: 8px;
  border: 1px solid
    ${(props) => (props.border ? props.border : 'var(--baseBorderGrey)')};
  color: var(--baseBlack);
  padding: ${(props) => (props.padding ? props.padding : '0.5rem')};
  background-color: ${(props) =>
    props.backgroundColor ? props.backgroundColor : 'var(--baseWhite)'};
  margin: ${(props) => (props.margin ? props.margin : '0')};
  width: 100%;
  ::placeholder {
    color: var(--baseFontGrey);
    opacity: 1;
  }
`
export const Button = styled.button`
  background-color: ${(props) =>
    props.backgroundColor ? props.backgroundColor : 'var(--baseBlue)'};
  font-weight: bold;
  color: ${(props) => (props.color ? props.color : 'var(--baseWhite)')};
  border-radius: 8px;
  display: block;
  width: ${(props) => (props.width ? props.width : '100%')};
  height: ${(props) => (props.height ? props.height : '100%')};
  border: none;
  padding: ${(props) => (props.padding ? props.padding : '0.8rem 1rem')};
  margin: ${(props) => (props.margin ? props.margin : '0')};
  font-size: ${(props) => (props.fontSize ? props.fontSize : '1rem')};
  cursor: pointer;
  :hover {
    opacity: 0.9;
  }
`
export const FlexBox = styled.div`
  position: relative;
  display: flex;
  flex-direction: ${(props) =>
    props.flexDirection ? props.flexDirection : 'row'};
  justify-content: ${(props) =>
    props.justifyContent ? props.justifyContent : 'flex-start'};
  align-items: ${(props) => (props.alignItems ? props.alignItems : 'center')};
  width: ${(props) => (props.width ? props.width : '100%')};
  height: ${(props) => (props.height ? props.height : '100%')};
  margin: ${(props) => (props.margin ? props.margin : '0')};
  padding: ${(props) => (props.padding ? props.padding : '0')};
`
export const FlexBoxWithBottomBorder = styled.div`
  display: flex;
  border-bottom: 1px solid var(--baseBorderGrey);
  flex-direction: ${(props) =>
    props.flexDirection ? props.flexDirection : 'row'};
  justify-content: ${(props) =>
    props.justifyContent ? props.justifyContent : 'flex-start'};
  align-items: ${(props) => (props.alignItems ? props.alignItems : 'center')};
  width: ${(props) => (props.width ? props.width : '100%')};
  margin: ${(props) => (props.margin ? props.margin : '0')};
  color: ${(props) => (props.color ? props.color : 'var(--baseFontBlue)')};
  cursor: ${(props) => (props.isHoverable ? 'pointer' : 'none')};
  opacity: ${(props) => (props.isOpactity ? 0.6 : 1)};
  padding: ${(props) => (props.padding ? props.padding : '0')};
  :hover {
    div {
      border-bottom: ${(props) => (props.isHoverable ? '1px' : '0px')} solid
        ${(props) =>
          props.isHoverable ? 'var(--baseFontBlue)' : 'transparent'};
    }
  }
`
export const Title = styled.h1`
  font-weight: bold;
  width: 100%;
  text-align: ${(props) => (props.textAlign ? props.textAlign : 'center')};
  font-size: ${(props) => (props.fontSize ? props.fontSize : '3rem')};
  margin: ${(props) => (props.margin ? props.margin : '0')};
  color: ${(props) => (props.color ? props.color : 'var(--baseBlue)')};
`
export const Text = styled.div`
  width: ${(props) => (props.width ? props.width : '100%')};
  font-weight: ${(props) => (props.isBold ? 'bold' : 'normal')};
  text-align: ${(props) => (props.textAlign ? props.textAlign : 'center')};
  font-size: ${(props) => (props.fontSize ? props.fontSize : '0.9rem')};
  color: ${(props) => (props.color ? props.color : 'var(--baseFontGrey)')};
  vertical-align: 'middle';
  margin: ${(props) => (props.margin ? props.margin : '0')};
`
export const ContentContainer = styled.div`
  padding: 1rem;
  margin: 0 0 1rem 0;
  width: 100%;
  background-color: var(--baseContainerDarkBg);
  border-radius: 8px;
  color: var(--baseFontGrey);
`
export const Form = styled.form`
  width: 100%;
`
export const Textarea = styled.textarea`
  width: 100%;
  background-color: var(--baseContainerDarkBg);
  margin: 1rem 0;
  border: none;
  font-size: 1.5rem;
  color: var(--baseWhite);
  outline: none;
  ::placeholder {
    color: var(--baseFontGrey);
  }
`
export const Close = styled(({ margin, ...props }) => (
  <CloseOutlined {...props} />
))`
  ${() => css`
    position: absolute;
    right: 0;
    top: 0;
    margin: ${(props) => (props.margin ? props.margin : '1rem 0.5rem')};
    font-size: 1.2rem;
    z-index: 2;
    background-color: var(--baseItemHoverDark);
    border-radius: 50%;
    color: var(--baseFontGrey);
    padding: 0.5rem;
    :hover {
      background-color: var(--baseItemHoverLighter);
    }
  `}
`
export const CardContainer = styled(({ width, ...props }) => (
  <Card {...props} />
))`
  ${() => css`
    background-color: var(--baseContainerDarkBg);
    max-width: ${(props) => (props.width ? props.width : '100%')};
    min-width: 'max-content';
    border-radius: 8px;
    border: none;
    padding: ${(props) => (props.padding ? props.padding : '0')};
    .ant-card-head {
      border-bottom: 1px solid var(--baseBorderDarkGrey);
      padding-left: 0;
      padding-right: 0;
    }
    .ant-card-body {
      padding-bottom: 0;
      padding-left: 0;
      padding-right: 0;
    }
    .ant-card-actions {
      display: flex;
      justify-content: space-evenly;
      align-items: center;
      border: none;
      padding: 0;
      background-color: var(--baseContainerDarkBg);
      li {
        border: none;
        margin: 0.5rem;
        button {
          :hover {
            background-color: var(--baseBorderDarkGrey);
          }
        }
      }
    }
  `}
`
export const AvatarWrapper = styled(({ margin, backgroundColor, ...props }) => (
  <Avatar {...props} />
))`
  ${() => css`
    margin: ${(props) => (props.margin ? props.margin : 0)};
    background-color: ${(props) =>
      props.backgroundColor ? props.backgroundColor : 'var(--baseYellowGreen)'};
  `}
`
export const ListItemMetaWrapper = styled(List.Item.Meta)`
  .ant-list-item-meta-content * {
    color: var(--baseFontGrey);
  }
`
