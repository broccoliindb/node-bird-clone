import { all, fork } from 'redux-saga/effects'
import userSagas from './user'
import postSagas from './post'
import axios from 'axios'
import { backUrl } from '../config/config'

axios.defaults.baseURL = backUrl
axios.defaults.withCredentials = true
export default function* rootSagas() {
  yield all([fork(userSagas), fork(postSagas)])
}
