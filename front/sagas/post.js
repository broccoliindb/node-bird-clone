import { takeLatest, all, fork, put, call, delay } from 'redux-saga/effects'
import Types from '../actions'
import axios from 'axios'
/**
 * @desc add post
 * @param
 * @return
 */
function addPostApi(data) {
  return axios.post('/post', data)
}
function* addPostRequest(action) {
  try {
    const post = yield call(addPostApi, action.data)
    yield put({ type: Types.ADD_POST_FULFILLED, payload: post.data })
    yield put({ type: Types.POST_MODAL_OFF })
  } catch (error) {
    yield put({
      type: Types.ADD_POST_REJECTED,
      error
    })
  }
}
function* watchAddPost() {
  yield takeLatest(Types.ADD_POST_REQUEST, addPostRequest)
}

/**
 * @desc remove post
 * @param
 * @return
 */
const removePostApi = () => {
  return axios.post('/api/removePost')
}
function* removePostRequest() {
  try {
    yield call(removePostApi)
    yield put({ type: Types.REMOVE_POST_FULFILLED })
  } catch (error) {
    yield put({
      type: Types.REMOVE_POST_REJECTED,
      error: error
    })
  }
}
function* watchRemovePost() {
  yield takeLatest(Types.REMOVE_POST_REQUEST, removePostRequest)
}

/**
 * @desc add comment
 * @param
 * @return
 */
const addCommentApi = (data) => {
  return axios.post(`/post/${data.postId}/comment`, data)
}
function* addCommentRequest(action) {
  try {
    const comment = yield call(addCommentApi, action.data)
    yield put({
      type: Types.ADD_COMMENT_FULFILLED,
      payload: comment.data
    })
  } catch (error) {
    yield put({
      type: Types.ADD_COMMENT_REJECTED,
      error
    })
  }
}
function* watchAddComment() {
  yield takeLatest(Types.ADD_COMMENT_REQUEST, addCommentRequest)
}

/**
 * @desc remove comment
 * @param
 * @return
 */
const removeCommentApi = () => {
  return axios.post('/api/removeComment')
}
function* removeCommentRequest() {
  try {
    yield call(removeCommentApi)
    yield put({ type: Types.REMOVE_POST_FULFILLED })
  } catch (error) {
    yield put({
      type: Types.REMOVE_COMMNET_REJECTED,
      error
    })
  }
}
function* watchRemoveComment() {
  yield takeLatest(Types.REMOVE_COMMNET_REQUEST, removeCommentRequest)
}

/**
 * @desc getPosts
 * @param
 * @return
 */
const getPostListApi = () => {
  return axios.get('/post')
}
function* getPostListRequest() {
  try {
    const posts = yield call(getPostListApi)
    yield put({ type: Types.GET_POST_LIST_FULFILLED, payload: posts.data })
  } catch (error) {
    yield put({
      type: Types.GET_POST_LIST_REJECTED,
      error
    })
  }
}
function* watchGetPosts() {
  yield takeLatest(Types.GET_POST_LIST_REQUEST, getPostListRequest)
}

/**
 * @desc like post
 * @param
 * @return
 */
const setLikePostApi = (postId) => {
  return axios.patch(`/post/${postId}/like`)
}
function* likePostRequest(action) {
  try {
    const post = yield call(setLikePostApi, action.data)
    yield put({ type: Types.LIKE_POST_FULFILLED, payload: post.data })
  } catch (error) {
    console.error(error)
    yield put({
      type: Types.LIKE_POST_REJECTED,
      error
    })
  }
}
function* watchLikePost() {
  yield takeLatest(Types.LIKE_POST_REQUEST, likePostRequest)
}

/**
 * @desc dislike post
 * @param
 * @return
 */
const setDislikePostApi = (postId) => {
  return axios.patch(`/post/${postId}/dislike`)
}
function* disLikePostRequest(action) {
  try {
    const posts = yield call(setDislikePostApi, action.data)
    yield put({ type: Types.DISLIKE_POST_FULFILLED, payload: posts.data })
  } catch (error) {
    yield put({
      type: Types.DISLIKE_POST_REJECTED,
      error
    })
  }
}
function* watchDislikePost() {
  yield takeLatest(Types.DISLIKE_POST_REQUEST, disLikePostRequest)
}

export default function* postSaga() {
  yield all([
    fork(watchAddPost),
    fork(watchRemovePost),
    fork(watchAddComment),
    fork(watchRemoveComment),
    fork(watchGetPosts),
    fork(watchLikePost),
    fork(watchDislikePost)
  ])
}
