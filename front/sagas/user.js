import Types from '../actions'
import { all, call, put, takeLatest, fork, delay } from 'redux-saga/effects'
import axios from 'axios'

/**
 * @desc login
 * @param
 * @return
 */
const loginApi = (data) => {
  return axios.post('/user/login', data)
}
function* loginRequest(action) {
  try {
    const user = yield call(loginApi, action.data)
    yield put({ type: Types.LOG_IN_FULFILLED, payload: user.data })
  } catch (error) {
    yield put({ type: Types.LOG_IN_REJECTED, error })
  }
}
function* watchLogin() {
  yield takeLatest(Types.LOG_IN_REQUEST, loginRequest)
}

/**
 * @desc logout
 * @param
 * @return
 */
const logoutApi = () => {
  return axios.post('/user/logout')
}
function* logoutRequest() {
  try {
    yield call(logoutApi)
    yield put({ type: Types.LOG_OUT_FULFILLED })
  } catch (error) {
    yield put({ type: Types.LOG_OUT_REJECTED, error })
  }
}
function* watchLogout() {
  yield takeLatest(Types.LOG_OUT_REQUEST, logoutRequest)
}

/**
 * @desc change nickname
 * @param
 * @return
 */
const changeNickNameApi = () => {
  return axios.post('/api/changeNickname')
}
function* changeNickNameRequest(action) {
  try {
    const nickname = yield call(changeNickNameApi, action.payload)
    yield put({
      type: Types.CHANGE_NICK_NAME_FULFILLED,
      paylaod: nickname.data
    })
  } catch (e) {
    yield put({
      type: Types.CHANGE_NICK_NAME_REJECTED,
      error: e
    })
  }
}
function* watchChangeNickname() {
  yield takeLatest(Types.CHANGE_NICK_NAME_REQUEST, changeNickNameRequest)
}

/**
 * @desc sign up
 * @param
 * @return
 */
const signUpApi = (data) => {
  return axios.post('/user/signup', data)
}
function* signUpRequest(action) {
  try {
    yield call(signUpApi, action.data)
    yield put({
      type: Types.SIGN_UP_FULFILLED,
      message: '회원가입에 성공했습니다.'
    })
  } catch (error) {
    yield put({
      type: Types.SIGN_UP_REJECTED,
      error
    })
  }
}
function* watchSignUp() {
  yield takeLatest(Types.SIGN_UP_REQUEST, signUpRequest)
}

/**
 * @desc withdraw account
 * @param
 * @return
 */
const withdrawAccountApi = () => {
  return axios.post('/api/withdrawApi')
}
function* withdrawAccountRequest() {
  try {
    // yield call(withdrawAccountApi)
    delay(1000)
    yield put({ type: Types.WITHDRAW_FULFILLED })
  } catch (error) {
    yield put({ type: Types.WITHDRAW_REJECTED, error })
  }
}
function* watchWithdraw() {
  yield takeLatest(Types.WITHDRAW_REJECTED, withdrawAccountRequest)
}

/**
 * @desc getUserInfoRequest
 * @param
 * @return
 */

const getUserInfoApi = () => {
  return axios.get('/user')
}

function* getUserInfoRequest() {
  try {
    const user = yield call(getUserInfoApi)
    yield put({ type: Types.GET_USER_INFO_FULFILLED, payload: user.data })
  } catch (error) {
    yield put({ type: Types.GET_USER_INFO_REJECTED, error })
  }
}

function* watchGetUserInfo() {
  yield takeLatest(Types.GET_USER_INFO_REQUEST, getUserInfoRequest)
}

/**
 * @desc getUserInfoRequest
 * @param
 * @return
 */

const changeUserInfoApi = (data) => {
  return axios.patch(`/user/${data.id}`, data)
}

function* changeUserInfoRequest(action) {
  try {
    const user = yield call(changeUserInfoApi, action.data)
    yield put({ type: Types.CHANGE_USER_INFO_FULFILLED, payload: user.data })
  } catch (error) {
    yield put({ type: Types.CHANGE_USER_INFO_REJECTED, error })
  }
}

function* watchChangeUserInfo() {
  yield takeLatest(Types.CHANGE_USER_INFO_REQUEST, changeUserInfoRequest)
}

/**
 * @desc FacebookLogin
 * @param
 * @return
 */

const facebookLoginApi = () => {
  return axios.get('/auth/facebook')
}

function* facebookLoginRequest() {
  try {
    yield call(facebookLoginApi)
    yield put({ type: Types.FACEBOOK_LOGIN_FULFILLED })
  } catch (error) {
    yield put({ type: Types.FACEBOOK_LOGIN_REJECTED, error })
  }
}

function* watchFacebookLogin() {
  yield takeLatest(Types.FACEBOOK_LOGIN_REQUEST, facebookLoginRequest)
}

export default function* userSaga() {
  yield all([
    fork(watchLogin),
    fork(watchLogout),
    fork(watchChangeNickname),
    fork(watchSignUp),
    fork(watchWithdraw),
    fork(watchGetUserInfo),
    fork(watchChangeUserInfo),
    fork(watchFacebookLogin)
  ])
}
