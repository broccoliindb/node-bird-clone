import { createWrapper } from 'next-redux-wrapper'
import { createStore, compose, applyMiddleware } from 'redux'
import reducer from '../reducers'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware, { Task } from 'redux-saga'
import rootSaga from '../sagas'

// const mid = ({ dispatch, state }) => (next) => (action) => {
//   return next(action)
// }

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware()
  const middleWares = [sagaMiddleware]

  // const enhancer =
  //   process.env.NODE_ENV === 'production'
  //     ? compose(applyMiddleware(...middleWares))
  //     : composeWithDevTools(applyMiddleware(...middleWares))
  const enhancer = composeWithDevTools(applyMiddleware(...middleWares))
  const store = createStore(reducer, enhancer)
  store.sagaTask = sagaMiddleware.run(rootSaga)
  return store
}

const wrapper = createWrapper(configureStore, {
  // debug: process.env.NODE_ENV === 'development'
  debug: true
})

export default wrapper
